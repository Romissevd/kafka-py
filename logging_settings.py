import os

import yaml

import logging.config
import logging


current_dir = os.path.dirname(__file__)
config_file_path = os.path.join(current_dir, 'logging.yaml')


def setup_logging():
    with open(config_file_path, 'rt') as _file:
        config = yaml.safe_load(_file.read())
        logging.config.dictConfig(config)
