import logging
import logging_settings
import socket

from confluent_kafka import Producer

logging_settings.setup_logging()
logger = logging.getLogger('kafka-py').getChild(__file__)

conf = {
    'bootstrap.servers': 'localhost:9092,localhost:9093,localhost:9094',
    'client.id': socket.gethostname(),
}


def acked(err, msg):
    if err is not None:
        logger.error('Failed to deliver message: %s: %s' % (str(msg), str(err)))
    else:
        logger.debug(
            'Message produced: topic - %s, partition - %s, offset - %s' % (msg.topic(), msg.partition(), msg.offset())
        )


topic = ''
while not topic:
    topic = input('Kafka topic name: ')

message_key = input('Message key: ')
message_value = input('Message value: ')

producer = Producer(conf)
producer.produce(topic, key=message_key, value=message_value, callback=acked)
producer.poll(1)
