import os

from confluent_kafka.schema_registry import SchemaRegistryClient

SCHEMA_FILENAME = 'alert.avsc'


path = os.path.realpath(os.path.dirname(__file__))
with open(f'{path}/{SCHEMA_FILENAME}') as f:
    avro_schema_str = f.read()

schema_registry_conf = {'url': 'http://0.0.0.0:8081'}
schema_registry_client = SchemaRegistryClient(schema_registry_conf)
