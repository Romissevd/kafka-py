import logging
from dataclasses import asdict
from uuid import uuid4

from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.serialization import StringSerializer, SerializationContext, MessageField

import logging_settings
import socket

from confluent_kafka import Producer

from avro.alert import Alert, AlertStatus
from avro.avro_schema import schema_registry_client, avro_schema_str
from avro.config import TOPIC_NAME, BOOTSTRAP_SERVERS
from avro.utils import get_partition_for_alert

logging_settings.setup_logging()
logger = logging.getLogger('kafka-py').getChild(__file__)

conf = {
    'bootstrap.servers': BOOTSTRAP_SERVERS,
    'client.id': socket.gethostname(),
}
# Additional settings
conf.update({
    'acks': 'all',
    'retries': '3',
    'max.in.flight.requests.per.connection': '3'
})


def alert_asdict(alert: Alert, ctx):
    return asdict(alert)


def delivery_report(err, msg):
    if err is not None:
        logger.error('Failed to deliver message: %s: %s' % (str(msg), str(err)))
    else:
        logger.debug(
            'Message produced: topic - %s, partition - %s, offset - %s' % (msg.topic(), msg.partition(), msg.offset())
        )


sensor_id = None
while not isinstance(sensor_id, int):
    sensor_id = input('Sensor id (integer): ')
    if not sensor_id.isdigit():
        continue
    sensor_id = int(sensor_id)


status = None
allowed_status = [status.value for status in AlertStatus]
while not isinstance(status, AlertStatus):
    status = input(f'Status (allowed statuses: {allowed_status}): ')
    status = status.capitalize()
    if status not in allowed_status:
        continue
    status = AlertStatus[status.upper()]


alert = Alert(sensor_id=sensor_id, status=status)

avro_serializer = AvroSerializer(schema_registry_client, avro_schema_str, to_dict=alert_asdict)
string_serializer = StringSerializer('utf_8')

partition = get_partition_for_alert(alert.status)
producer = Producer(conf)
producer.produce(
    topic=TOPIC_NAME,
    partition=partition,
    key=string_serializer(str(uuid4())),
    value=avro_serializer(alert, SerializationContext(TOPIC_NAME, MessageField.VALUE)),
    on_delivery=delivery_report
)
producer.poll(1)
