import logging

from confluent_kafka import Consumer, KafkaException, KafkaError
from confluent_kafka.schema_registry.avro import AvroDeserializer
from confluent_kafka.serialization import SerializationContext, MessageField, SerializationError

import logging_settings
from avro.alert import Alert
from avro.avro_schema import avro_schema_str, schema_registry_client
from avro.config import TOPIC_NAME, BOOTSTRAP_SERVERS
from avro.utils import timestamp_to_human_time

conf = {
    'bootstrap.servers': BOOTSTRAP_SERVERS,
    'group.id': 'foo',
    'enable.auto.commit': 'false',
    'auto.offset.reset': 'earliest'
}

consumer = Consumer(conf)

running = True

MIN_COMMIT_COUNT = 3

logging_settings.setup_logging()
logger = logging.getLogger('kafka-py').getChild(__file__)


def dict_to_alert(obj, ctx):
    if obj is None:
        return None

    return Alert(
        sensor_id=obj['sensor_id'],
        status=obj['status'],
        time=obj['time'],
    )


avro_deserializer = AvroDeserializer(schema_registry_client, avro_schema_str, from_dict=dict_to_alert)


def consume_loop(consumer: Consumer, topics: list[str]):
    try:
        consumer.subscribe(topics)

        msg_count = 0
        while running:
            msg = consumer.poll(timeout=1.0)
            if msg is None:
                continue

            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # End of partition event
                    logger.error(
                        '%s [%d] reached end at offset %d.' % (msg.topic(), msg.partition(), msg.offset())
                    )
                elif msg.error():
                    logger.error('Message error - %s. Raise KafkaException.', msg.error())
                    raise KafkaException(msg.error())
            else:
                try:
                    alert = avro_deserializer(msg.value(), SerializationContext(msg.topic(), MessageField.VALUE))
                except SerializationError:
                    logger.error(
                        'Serialization message error. Message received - key: %s, value: %s.',
                        msg.key(), msg.value()
                    )
                    continue
                if alert is not None:
                    logger.debug(
                        'Alert record %s: sensor_id: %s, status: %s, time: %s.',
                        msg.key(), alert.sensor_id, alert.status, timestamp_to_human_time(alert.time)
                    )
                msg_count += 1
                if msg_count % MIN_COMMIT_COUNT == 0:
                    consumer.commit(asynchronous=True)
    finally:
        # Close down consumer to commit final offsets.
        consumer.close()


consume_loop(consumer, [TOPIC_NAME])
