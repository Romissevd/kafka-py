import enum
from dataclasses import dataclass, field
from datetime import datetime, timezone


class AlertStatus(enum.StrEnum):
    CRITICAL = 'Critical'
    MAJOR = 'Major'
    MINOR = 'Minor'
    WARNING = 'Warning'


@dataclass
class Alert:
    sensor_id: int
    status: AlertStatus
    time: int = field(default_factory=lambda: int(round(datetime.timestamp(datetime.now(tz=timezone.utc)))))

    def __post_init__(self):
        if self.status not in list(AlertStatus):
            raise ValueError('status id not a valid value')
