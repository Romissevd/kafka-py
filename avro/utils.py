from datetime import datetime

from avro.alert import AlertStatus


def timestamp_to_human_time(timestamp: int | float, format_string='%Y-%m-%d %H:%M:%S') -> str:
    return datetime.fromtimestamp(timestamp).strftime(format_string)


def get_partition_for_alert(status: AlertStatus) -> int:
    converter = {
        AlertStatus.CRITICAL: 0,
        AlertStatus.MAJOR: 1,
        AlertStatus.MINOR: 2,
        AlertStatus.WARNING: 2,
    }
    return converter.get(status, 0)
