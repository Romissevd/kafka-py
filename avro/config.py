from confluent_kafka import Producer, admin

BOOTSTRAP_SERVERS = 'localhost:9092,localhost:9093,localhost:9094'
TOPIC_NAME = 'avro_confluent_kafka'

kafka_admin = admin.AdminClient({'bootstrap.servers': BOOTSTRAP_SERVERS})
topic = admin.NewTopic(TOPIC_NAME, 3, 3)
res = kafka_admin.create_topics([topic])
