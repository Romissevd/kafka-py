import logging

from confluent_kafka import Consumer, KafkaException, KafkaError

import logging_settings

conf = {
    'bootstrap.servers': 'localhost:9092,localhost:9093,localhost:9094',
    'group.id': 'foo',
    'enable.auto.commit': 'false',
    'auto.offset.reset': 'earliest'
}

consumer = Consumer(conf)

running = True

MIN_COMMIT_COUNT = 3

logging_settings.setup_logging()
logger = logging.getLogger('kafka-py').getChild(__file__)


def consume_loop(consumer: Consumer, topics: list[str]):
    try:
        consumer.subscribe(topics)

        msg_count = 0
        while running:
            msg = consumer.poll(timeout=1.0)
            if msg is None:
                continue

            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # End of partition event
                    logger.error(
                        '%s [%d] reached end at offset %d.' % (msg.topic(), msg.partition(), msg.offset())
                    )
                elif msg.error():
                    logger.error('Message error - %s. Raise KafkaException.', msg.error())
                    raise KafkaException(msg.error())
            else:
                msg_process(msg)
                msg_count += 1
                if msg_count % MIN_COMMIT_COUNT == 0:
                    consumer.commit(asynchronous=True)
    finally:
        # Close down consumer to commit final offsets.
        consumer.close()


def msg_process(msg):
    print('Message received - ', 'key: ', msg.key(), ', value: ', msg.value())


consume_loop(consumer, ['kinaction_helloworld'])
